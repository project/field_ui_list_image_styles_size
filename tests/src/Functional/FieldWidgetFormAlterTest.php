<?php

namespace Drupal\Tests\field_ui_list_image_styles_size\Functional;

/**
 * Class FieldWidgetFormAlterTest.
 *
 * Tests that the forms are altered.
 *
 * @package Drupal\Tests\field_ui_list_image_styles_size\Functional
 * @group field_ui_list_image_styles_size
 */
class FieldWidgetFormAlterTest extends TestBase {

  /**
   * TestFormAlter.
   */
  public function testFormAlter(): void {
    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/edit');

    $session = $this->assertSession();
    $field_image_label = $session->elementExists('css', '#edit-field-image-0--label');

    $this->assertEqual('field_image - 123x456', $field_image_label->getText());

    $session->elementExists('css', '#edit-field-image-0-field-ui-list-image-styles-size');
  }

  /**
   * TestFormAlterWithoutListBelow.
   */
  public function testFormAlterWithoutListBelow(): void {
    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', FALSE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/edit');

    $session = $this->assertSession();
    $session->elementNotExists('css', '#edit-field-image-0-field-ui-list-image-styles-size');
  }

  /**
   * TestFormAlterNotOnFrontendTheme.
   */
  public function testFormAlterNotOnFrontendTheme(): void {
    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', FALSE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/edit');

    $session = $this->assertSession();
    $field_image_label = $session->elementExists('css', '#edit-field-image-0--label');

    $this->assertEqual('field_image', $field_image_label->getText());

    $session->elementNotExists('css', '#edit-field-image-0-field-ui-list-image-styles-size');
  }

  /**
   * TestFormAlterAlternativeLabel.
   */
  public function testFormAlterAlternativeLabel(): void {
    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->fieldImageConfig->setThirdPartySetting('field_ui_list_image_styles_size', 'append_view_mode_size_to_field_title', 'alternative');
    $this->fieldImageConfig->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/edit');

    $session = $this->assertSession();
    $field_image_label = $session->elementExists('css', '#edit-field-image-0--label');

    $this->assertEqual('field_image - 1000x?', $field_image_label->getText());

    $session->elementExists('css', '#edit-field-image-0-field-ui-list-image-styles-size');
  }

  /**
   * TestFormAlterNoLabel.
   */
  public function testFormAlterNoLabel(): void {
    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->fieldImageConfig->setThirdPartySetting('field_ui_list_image_styles_size', 'append_view_mode_size_to_field_title', '');
    $this->fieldImageConfig->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/edit');

    $session = $this->assertSession();
    $field_image_label = $session->elementExists('css', '#edit-field-image-0--label');

    $this->assertEqual('field_image', $field_image_label->getText());

    $session->elementExists('css', '#edit-field-image-0-field-ui-list-image-styles-size');
  }

}
