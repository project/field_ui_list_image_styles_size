<?php

namespace Drupal\Tests\field_ui_list_image_styles_size\Functional;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;
use Drupal\Tests\BrowserTestBase;

/**
 * Class TestBase.
 *
 * Test base class.
 *
 * @package Drupal\Tests\field_ui_list_image_styles_size\Functional
 */
abstract class TestBase extends BrowserTestBase {

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to load.
   *
   * @var string[]
   */
  public static $modules = [
    'user',
    'field',
    'text',
    'image',
    'node',
    'field_ui',
    'field_ui_list_image_styles_size',
  ];

  /**
   * Entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * No image style view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $noImageStyleViewDisplay;

  /**
   * Default view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $defaultViewDisplay;

  /**
   * Alternative view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $alternativeViewDisplay;

  /**
   * Form display.
   *
   * @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  protected $formDisplay;

  /**
   * Module settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $fieldUiListImageStylesSettingsConfig;

  /**
   * Test node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Resize image style.
   *
   * @var \Drupal\image\ImageStyleInterface
   */
  protected $resizeImageStyle;

  /**
   * Resize (width) image style.
   *
   * @var \Drupal\image\ImageStyleInterface
   */
  protected $resizeWithoutHeightImageStyle;

  /**
   * A user with permission to bypass content access checks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Field_image field config.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $fieldImageConfig;

  /**
   * {@inheritDoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->entityDisplayRepository = $this->container->get('entity_display.repository');
    $this->fieldUiListImageStylesSettingsConfig = $this->config('field_ui_list_image_styles_size.settings');

    $this->fieldUiListImageStylesSettingsConfig->set('show_frontend_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_administration_theme', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->set('show_viewmodes_list_below_field', TRUE);
    $this->fieldUiListImageStylesSettingsConfig->save();

    $this->drupalCreateContentType([
      'type' => 'test',
      'name' => 'test',
    ]);

    // field_text1.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_text1',
      'type' => 'text_long',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'test',
      'field_name' => 'field_text1',
      'label' => 'field_text1',
      'required' => FALSE,
    ]);
    $field->save();

    // field_image.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => 'field_image',
      'type' => 'image',
      'cardinality' => 1,
    ]);
    $field_storage->save();
    $this->fieldImageConfig = FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'test',
      'field_name' => 'field_image',
      'label' => 'field_image',
      'required' => FALSE,
    ]);
    $this->fieldImageConfig->setThirdPartySetting('field_ui_list_image_styles_size', 'append_view_mode_size_to_field_title', 'default');
    $this->fieldImageConfig->save();

    $this->resizeImageStyle = ImageStyle::create([
      'name' => 'imageStyle',
      'label' => 'imageStyle',
    ]);
    $this->resizeImageStyle->addImageEffect([
      'id' => 'image_resize',
      'data' => [
        'width' => 123,
        'height' => 456,
      ],
    ]);
    $this->resizeImageStyle->save();

    $this->resizeWithoutHeightImageStyle = ImageStyle::create([
      'name' => 'resizeWithoutHeightImageStyle',
      'label' => 'resizeWithoutHeightImageStyle',
    ]);
    $this->resizeWithoutHeightImageStyle->addImageEffect([
      'id' => 'image_resize',
      'data' => [
        'width' => 1000,
      ],
    ]);
    $this->resizeWithoutHeightImageStyle->save();

    $noImageStyleEntityViewMode = EntityViewMode::create([
      'id' => 'node.noimagestyle',
      'targetEntityType' => 'node',
    ]);
    $noImageStyleEntityViewMode->save();

    $this->noImageStyleViewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => 'test',
      'mode' => 'noimagestyle',
      'status' => TRUE,
    ]);
    $this->noImageStyleViewDisplay->setComponent('field_image', [
      'label' => 'above',
      'type' => 'image',
    ]);
    $this->noImageStyleViewDisplay->save();

    $this->defaultViewDisplay = $this->entityDisplayRepository->getViewDisplay('node', 'test', EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE);

    $this->defaultViewDisplay->setComponent('field_text1', [
      'label' => 'above',
      'type' => 'text_default',
    ]);
    $this->defaultViewDisplay->setComponent('field_image', [
      'label' => 'above',
      'type' => 'image',
      'settings' => [
        'image_style' => $this->resizeImageStyle->id(),
      ],
    ]);
    $this->defaultViewDisplay->save();

    $alternativeEntityViewMode = EntityViewMode::create([
      'id' => 'node.alternative',
      'targetEntityType' => 'node',
    ]);
    $alternativeEntityViewMode->save();

    $this->alternativeViewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => 'test',
      'mode' => 'alternative',
      'status' => TRUE,
    ]);
    $this->alternativeViewDisplay->setComponent('field_image', [
      'label' => 'above',
      'type' => 'image',
      'settings' => [
        'image_style' => $this->resizeWithoutHeightImageStyle->id(),
      ],
    ]);
    $this->alternativeViewDisplay->save();

    $this->formDisplay = $this->entityDisplayRepository->getFormDisplay('node', 'test');

    $this->formDisplay->setComponent('field_text1', [
      'type' => 'text_textfield',
    ]);
    $this->formDisplay->setComponent('field_image', [
      'type' => 'image_image',
    ]);
    $this->formDisplay->save();

    $testimage = file_save_data(file_get_contents(__DIR__ . '/test.jpg'), 'public://test.jpg');

    $this->node = $this->drupalCreateNode([
      'type' => 'test',
      'field_text1' => 'Test text',
      'field_image' => [
        'target_id' => $testimage->id(),
      ],
    ]);

    $this->adminUser = $this->drupalCreateUser([
      'bypass node access',
      'administer nodes',
    ]);
  }

}
