<?php

namespace Drupal\Tests\field_ui_list_image_styles_size\Functional\Service;

use Drupal\Tests\field_ui_list_image_styles_size\Functional\TestBase;

/**
 * Class ImageSizeServiceTest.
 *
 * Tests the implementation of the image size service.
 *
 * @package Drupal\Tests\field_ui_list_image_styles_size\Functional\Service
 * @group field_ui_list_image_styles_size
 */
class ImageSizeServiceTest extends TestBase {

  /**
   * Image size service.
   *
   * @var \Drupal\field_ui_list_image_styles_size\Service\ImageSizeService
   */
  protected $imageSizeService;

  /**
   * {@inheritDoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->imageSizeService = $this->container->get('field_ui_list_image_styles_size.imagesize');
  }

  /**
   * TestGetAppliedImageStyles.
   */
  public function testGetAppliedImageStyles(): void {
    $appliedImageStyles = $this->imageSizeService->getAppliedImageStyles('node', 'test', 'field_text1');
    $this->assertCount(0, $appliedImageStyles);

    $appliedImageStyles = $this->imageSizeService->getAppliedImageStyles('node', 'test', 'field_image');

    $this->assertCount(2, $appliedImageStyles);
    $this->assertEqual($this->resizeImageStyle->id(), $appliedImageStyles['default']->id());
    $this->assertEqual($this->resizeWithoutHeightImageStyle->id(), $appliedImageStyles['alternative']->id());
  }

  /**
   * GetEntityViewDisplaysAppliedImageSize.
   */
  public function testGetEntityViewDisplaysAppliedImageSize(): void {
    $entity_view_displays_applied_image_size = $this->imageSizeService->getEntityViewDisplaysAppliedImageSize('node', 'test', 'field_text1');
    $this->assertCount(0, $entity_view_displays_applied_image_size);

    $entity_view_displays_applied_image_size = $this->imageSizeService->getEntityViewDisplaysAppliedImageSize('node', 'test', 'field_image');
    $this->assertCount(2, $entity_view_displays_applied_image_size);

    $this->assertArrayHasKey('default', $entity_view_displays_applied_image_size);
    $this->assertEqual($this->resizeImageStyle->id(), $entity_view_displays_applied_image_size['default']->imageStyle->id());
    $this->assertEqual(123, $entity_view_displays_applied_image_size['default']->width);
    $this->assertEqual(456, $entity_view_displays_applied_image_size['default']->height);
    $this->assertEqual('123x456', $entity_view_displays_applied_image_size['default']->getDimensionsAsString());

    $this->assertArrayHasKey('alternative', $entity_view_displays_applied_image_size);
    $this->assertEqual($this->resizeWithoutHeightImageStyle->id(), $entity_view_displays_applied_image_size['alternative']->imageStyle->id());
    $this->assertEqual(1000, $entity_view_displays_applied_image_size['alternative']->width);
    $this->assertNull($entity_view_displays_applied_image_size['alternative']->height);
    $this->assertEqual('1000x?', $entity_view_displays_applied_image_size['alternative']->getDimensionsAsString());
  }

}
