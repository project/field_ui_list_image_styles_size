<?php

namespace Drupal\field_ui_list_image_styles_size;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\image\ImageStyleInterface;

/**
 * Class EntityViewDisplayImageSize.
 *
 * @package Drupal\field_ui_list_image_styles_size
 */
class EntityViewDisplayAppliedImageSize {

  /**
   * The entity view display this applies to.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  public $entityViewDisplay;

  /**
   * The image style applied.
   *
   * @var \Drupal\image\ImageStyleInterface|null
   */
  public $imageStyle;

  /**
   * The image width.
   *
   * @var int|null
   */
  public $width;

  /**
   * The image height.
   *
   * @var int|null
   */
  public $height;

  /**
   * EntityViewDisplayAppliedImageSize constructor.
   *
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $entityViewDisplay
   *   The entity view display this applies to.
   * @param \Drupal\image\ImageStyleInterface|null $imageStyle
   *   The image style applied.
   * @param int|null $width
   *   The resulting width.
   * @param int|null $height
   *   The resulting height.
   */
  public function __construct(EntityViewDisplayInterface $entityViewDisplay, ImageStyleInterface $imageStyle = NULL, int $width = NULL, int $height = NULL) {
    $this->entityViewDisplay = $entityViewDisplay;
    $this->imageStyle = $imageStyle;
    $this->width = $width;
    $this->height = $height;
  }

  /**
   * Get the width/height dimensions as a user readable string.
   *
   * @return string
   *   The dimensions as a {width}x{height} string.
   */
  public function getDimensionsAsString(): string {
    $dimensions = [
      $this->width !== NULL ? $this->width : '?',
      $this->height !== NULL ? $this->height : '?',
    ];

    return implode('x', $dimensions);
  }

}
