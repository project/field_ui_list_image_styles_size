<?php

namespace Drupal\field_ui_list_image_styles_size\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * Form to configure the settings for field_ui_list_image_styles_size.
 *
 * @package Drupal\field_ui_list_image_styles_size\Form
 */
class SettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'field_ui_list_image_styles_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('field_ui_list_image_styles_size.settings');

    $form['show'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Visibility'),
      '#description' => $this->t('Choose where and how to show the image sizes information.'),
    ];

    $form['show']['show_frontend_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show in frontend theme'),
      '#default_value' => $config->get('show_frontend_theme'),
    ];

    $form['show']['show_administration_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show in administration theme'),
      '#default_value' => $config->get('show_administration_theme'),
    ];

    $form['show']['show_viewmodes_list_below_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show list of all view modes below the field.'),
      '#default_value' => $config->get('show_viewmodes_list_below_field'),
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('field_ui_list_image_styles_size.settings');

    $config->set('show_frontend_theme', $form_state->getValue('show_frontend_theme'));
    $config->set('show_administration_theme', $form_state->getValue('show_administration_theme'));
    $config->set('show_viewmodes_list_below_field', $form_state->getValue('show_viewmodes_list_below_field'));

    $config->save();

    $this->messenger()->addStatus($this->t('The settings have been saved.'));
  }

}
