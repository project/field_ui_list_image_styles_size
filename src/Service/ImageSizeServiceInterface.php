<?php

namespace Drupal\field_ui_list_image_styles_size\Service;

/**
 * Interface ImageSizeServiceInterface.
 *
 * Defines the interface for the Image Size Service.
 *
 * @package Drupal\field_ui_list_image_styles_size\Service
 */
interface ImageSizeServiceInterface {

  /**
   * Get all applied image styles by view mode for the given entity field.
   *
   * @param string $entityTypeId
   *   Entity type id.
   * @param string $bundle
   *   Entity bundle.
   * @param string $fieldName
   *   The field name.
   *
   * @return \Drupal\image\ImageStyleInterface[]|null[]
   *   An array of applied image styles by view mode.
   */
  public function getAppliedImageStyles(string $entityTypeId, string $bundle, string $fieldName): array;

  /**
   * List all view modes with their image sizes for the given entity.
   *
   * @param string $entityTypeId
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   * @param string $fieldName
   *   The field.
   *
   * @return \Drupal\field_ui_list_image_styles_size\EntityViewDisplayAppliedImageSize[]
   *   The applied image sizes per entity view display.
   */
  public function getEntityViewDisplaysAppliedImageSize(string $entityTypeId, string $bundle, string $fieldName): array;

}
