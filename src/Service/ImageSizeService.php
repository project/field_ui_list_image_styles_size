<?php

namespace Drupal\field_ui_list_image_styles_size\Service;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field_ui_list_image_styles_size\EntityViewDisplayAppliedImageSize;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\ImageEffect\ResizeImageEffect;

/**
 * Class ImageSizeService.
 *
 * Implements the image size service which can be used to determine which
 * image styles (and resulting sizes) are applied to an entity's field.
 *
 * @package Drupal\field_ui_list_image_styles_size\Service
 */
class ImageSizeService implements ImageSizeServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Image styles storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * GetAppliedImageStyles cache.
   *
   * @var mixed[]
   */
  protected $getAppliedImageStylesCache = [];

  /**
   * GetEntityViewDisplaysAppliedImageSize cache.
   *
   * @var mixed[]
   */
  protected $getEntityViewDisplaysAppliedImageSizeCache = [];

  /**
   * ImageSizeService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityDisplayRepositoryInterface $entityDisplayRepository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
    $this->entityDisplayRepository = $entityDisplayRepository;
  }

  /**
   * {@inheritDoc}
   */
  public function getAppliedImageStyles(string $entityTypeId, string $bundle, string $fieldName): array {
    $cache_key = $entityTypeId . '-' . $bundle . '-' . $fieldName;

    if (isset($this->getAppliedImageStylesCache[$cache_key])) {
      return $this->getAppliedImageStylesCache[$cache_key];
    }

    $applied_image_styles = [];
    foreach ($this->entityDisplayRepository->getViewModeOptionsByBundle($entityTypeId, $bundle) as $view_mode => $view_mode_label) {
      $view_display = $this->entityDisplayRepository->getViewDisplay($entityTypeId, $bundle, $view_mode);

      $component = $view_display->getComponent($fieldName);

      if ($component === NULL) {
        // $fieldName not visible in $view_mode.
        continue;
      }

      if (!isset($component['settings']['image_style'])) {
        // No image_style applied.
        continue;
      }

      /**
       * @var \Drupal\image\ImageStyleInterface|null $image_style
       */
      $image_style = $this->imageStyleStorage->load($component['settings']['image_style']);

      if ($image_style === NULL) {
        // Image style not found.
        continue;
      }

      $applied_image_styles[$view_mode] = $image_style;
    }

    $this->getAppliedImageStylesCache[$cache_key] = $applied_image_styles;

    return $applied_image_styles;
  }

  /**
   * Determine the image size of an image processed by the given image style.
   *
   * @param \Drupal\image\ImageStyleInterface $imageStyle
   *   The image style.
   *
   * @return int[]|null[]
   *   The width/height of an image processed by the given image style.
   */
  protected function determineSizeForImageStyle(ImageStyleInterface $imageStyle): array {
    $width = NULL;
    $height = NULL;

    foreach ($imageStyle->getEffects() as $effect) {
      $configuration = $effect->getConfiguration();

      if ($effect instanceof ResizeImageEffect) {
        if (!empty($configuration['data']['width'])) {
          $width = $configuration['data']['width'];
        }
        if (!empty($configuration['data']['height'])) {
          $height = $configuration['data']['height'];
        }
      }
    }

    return [
      $width,
      $height,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityViewDisplaysAppliedImageSize(string $entityTypeId, string $bundle, string $fieldName): array {
    $cache_key = $entityTypeId . '-' . $bundle . '-' . $fieldName;

    if (isset($this->getEntityViewDisplaysAppliedImageSizeCache[$cache_key])) {
      return $this->getEntityViewDisplaysAppliedImageSizeCache[$cache_key];
    }

    $applied_image_styles = $this->getAppliedImageStyles($entityTypeId, $bundle, $fieldName);

    $entity_view_displays_applied_image_size = [];

    foreach ($applied_image_styles as $view_mode => $image_style) {
      $entity_view_display = $this->entityDisplayRepository->getViewDisplay($entityTypeId, $bundle, $view_mode);
      if ($image_style !== NULL) {
        list($width, $height) = $this->determineSizeForImageStyle($image_style);
      }
      else {
        $width = NULL;
        $height = NULL;
      }

      $entity_view_displays_applied_image_size[$view_mode] = new EntityViewDisplayAppliedImageSize($entity_view_display, $image_style, $width, $height);
    }

    $this->getEntityViewDisplaysAppliedImageSizeCache[$cache_key] = $entity_view_displays_applied_image_size;

    return $entity_view_displays_applied_image_size;
  }

}
