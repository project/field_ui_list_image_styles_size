Field UI List Image Styles

Our content editors always wanted to know which (edited) image size is used in
the website for specific fields. This module shows the (applied) image
dimensions to use when editing an entity by adding it to the field title.

You can select an entity view mode of which you want to show the applied view
mode image style resized dimensions for each image or media field. There is
also an option to show a full list of all view modes and their applied image
style resizing dimensions.
